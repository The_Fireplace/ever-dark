package the_fireplace.everdark;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid=EverDark.MODID, name=EverDark.MODNAME, version=EverDark.VERSION, acceptedMinecraftVersions="1.8")
public class EverDark {
	@Instance(EverDark.VERSION)
	public static EverDark instance;
	public static final String MODID="everdark";
	public static final String MODNAME="EverDark";
	public static final String VERSION="2.0.1.0";
	@EventHandler
	public void preInit(FMLPreInitializationEvent event){
		FMLCommonHandler.instance().bus().register(new FMLEvents());
	}
}
